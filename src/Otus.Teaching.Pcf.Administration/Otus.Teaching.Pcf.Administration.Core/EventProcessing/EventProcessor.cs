﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.Core.Domain.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;

namespace Otus.Teaching.Pcf.Administration.Core.EventProcessing
{
    public class EventProcessor : IEventProcessor
    {
        private readonly IServiceScopeFactory _scopeFactory;

        public EventProcessor(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
        }

        public void ProcessEvent(string message)
        {
            var eventType = DetermineEvent(message);

            switch (eventType)
            {
                case EventType.Promocode_Published:
                    break;
                case EventType.Admin_Notified:
                    updatePromoCodesAsync(message);
                    break;
                case EventType.Undetermined:
                    break;
                default:
                    break;
            }
        }

        private async void updatePromoCodesAsync(string notificationMessage)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var employeeRepo = scope.ServiceProvider.GetRequiredService<IRepository<Employee>>();

                var adminNotifiedDto = JsonSerializer.Deserialize<AdminNotifiedDto>(notificationMessage);

                try
                {
                    var employee = await employeeRepo.GetByIdAsync(adminNotifiedDto.AdminId.Value);

                    employee.AppliedPromocodesCount++;
                    

                    await employeeRepo.UpdateAsync(employee);
                    Console.WriteLine($"--> Updated promocodes");
                }
                catch (Exception e)
                {
                    Console.WriteLine($"--> Could not update PromoCodes: {e.Message}");
                    throw;
                }
            }
        }

        private EventType DetermineEvent(string notificationMessage)
        { 
            Console.WriteLine("--> Determining Event");
            var eventType = JsonSerializer.Deserialize<GenericEventDto>(notificationMessage);

            switch(eventType.Event)
            {
                case "Promocode_Published":
                    Console.WriteLine("--> Promocode_Published Event detected");
                    return EventType.Promocode_Published;
                case "Admin_Notified":
                    Console.WriteLine("--> Admin_Notified Event detected");
                    return EventType.Admin_Notified;
                default:
                    Console.WriteLine("--> Event type not determined");
                    return EventType.Undetermined;
            }
        }

    }

    public enum EventType
    {
        Promocode_Published,
        Admin_Notified,
        Undetermined
    }
}
