﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto
{
    public class GivePromoCodeToCustomerDto
    {
        public GivePromoCodeToCustomerDto(PromoCode promoCode)
        {
            PartnerManagerId = promoCode.PartnerManagerId;
            ServiceInfo = promoCode.ServiceInfo;
            PartnerId = promoCode.PartnerId;
            PromoCodeId = promoCode.Id;
            PreferenceId = promoCode.PreferenceId;
            BeginDate = promoCode.BeginDate.ToLongDateString();
            EndDate = promoCode.EndDate.ToLongDateString();
        }

        public string ServiceInfo { get; set; }

        public Guid PartnerId { get; set; }

        public Guid PromoCodeId { get; set; }
        
        public string PromoCode { get; set; }

        public Guid PreferenceId { get; set; }

        public string BeginDate { get; set; }

        public string EndDate { get; set; }
        
        public Guid? PartnerManagerId { get; set; }

        public string Event { get; set; }
    }
}